# MinePKG

This is a package manager for minetest mods

## Usage

Fetching the newest mod list:

```shell-session
$ minepkg update
```

Installing mods:

```shell-session
$ minepkg add moreblocks basic_materials
Installing packages:
- moreblocks
- basic_materials

Installing to /home/martijn/.minetest/mods
Continue installing? [Y/n]
moreblocks: 39.0KB [00:00, 326KB/s]
basic_materials: 39.0KB [00:00, 389KB/s]                                                                                                                                                                   
Installing moreblocks...
Installing basic_materials...
```